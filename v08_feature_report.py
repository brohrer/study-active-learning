from copy import deepcopy
import os
import numpy as np
import matplotlib.pyplot as plt
import cottonwood.experimental.visualize_structure as vst
from cottonwood.operations import Constant


def generate_report(model):
    model = deepcopy(model)
    image_path = "reports"
    os.makedirs(image_path, exist_ok=True)

    # print(model)
    create_structure_diagram = False
    if create_structure_diagram:
        print()
        print("Generating block diagram...")
        vst.render(model)

    n_features = model.blocks["naive_bayes"].n_features
    nb_block = deepcopy(model.blocks["naive_bayes"])
    model.remove("naive_bayes")
    data_block = deepcopy(model.blocks["testing_data"])
    model.remove("testing_data")

    # Select some features to visualize.
    n_feature_viz = 10
    i_features = np.random.choice(
        n_features, size=n_feature_viz, replace=False)

    for i_feature in i_features:

        # Create a single active feature signal.
        feature_activities = np.zeros(n_features)
        feature_activities[int(i_feature)] = 1
        img = model.backward_pass(feature_activities) * 4 + .5
        img = np.maximum(img, 0)
        img = np.minimum(img, 1)

        fig = plt.figure()
        ax = fig.gca()
        ax.imshow(img, vmin=0, vmax=1)
        plt.savefig(os.path.join(image_path, f"feature_{i_feature}.png"))
        plt.close()
