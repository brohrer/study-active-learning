from copy import deepcopy
import os
import sys
import termios
import tty
import numpy as np
import matplotlib.pyplot as plt
import cottonwood.experimental.visualize_structure as vst
from cottonwood.operations import Constant

orig_settings = termios.tcgetattr(sys.stdin)

def generate_report(model):
    model = deepcopy(model)
    image_path = "reports"
    os.makedirs(image_path, exist_ok=True)

    # print(model)
    create_structure_diagram = False
    if create_structure_diagram:
        print()
        print("Generating block diagram...")
        vst.render(model)

    n_features = model.blocks["naive_bayes"].n_features
    nb_block = deepcopy(model.blocks["naive_bayes"])
    model.remove("naive_bayes")
    data_block = deepcopy(model.blocks["testing_data"])
    model.remove("testing_data")

    # Select some features to visualize.
    n_feature_viz = 10
    i_features = np.random.choice(
        n_features, size=n_feature_viz, replace=False)

    for i_feature in i_features:

        # Create a single active feature signal.
        feature_activities = np.zeros(n_features)
        feature_activities[int(i_feature)] = 1
        img = model.backward_pass(feature_activities) * 4 + .5
        img = np.maximum(img, 0)
        img = np.minimum(img, 1)

        fig = plt.figure()
        ax = fig.gca()
        ax.imshow(img, vmin=0, vmax=1)
        plt.savefig(os.path.join(image_path, f"feature_{i_feature}.png"))
        plt.close()


def initiate_keypress_grabbing():
    tty.setcbreak(sys.stdin)


def terminate_keypress_grabbing():
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, orig_settings)


def initialize_labeling_figure():
    fig_ui = plt.figure()
    plt.ion()
    plt.show()
    ax_ui = fig_ui.gca()
    ax_ui.imshow(np.zeros((5, 5)))
    ax_ui.tick_params(bottom=False, top=False, left=False, right=False)
    ax_ui.tick_params(
        labelbottom=False, labeltop=False, labelleft=False, labelright=False)
    print()
    return ax_ui


def clear_labeling_figure(ax):
    ax.imshow(np.zeros((5, 5)))
    plt.draw()
    plt.pause(0.001)

def show_result(image, centers, categories, confidences):
    results_filename = os.path.join("reports", "result.png")
    fig = plt.figure()

    ax = fig.add_axes((.1, .1, .8, .8))
    ax.imshow(image, alpha=.3)
    for i in range(len(centers)):
        center = centers[i]
        category = categories[i]
        confidence = confidences[i]
        if category == 0:
            color = "black"
            size = 20
            linewidth = 1
        elif category == 1:
            color = "xkcd:kelly green"
            size = 100
            linewidth = 3
        else:
            color = "white"
            size = 20
            linewidth = 1
        ax.scatter(
            center[1],
            center[0],
            edgecolor=color,
            linewidth=linewidth,
            marker="o",
            facecolor="none",
            s=size,
            alpha=confidence,
            zorder=-3)
        ax.scatter(
            center[1],
            center[0],
            edgecolor=color,
            linewidth=.5,
            marker="o",
            facecolor="none",
            s=size,
            zorder=-3)
    ax.tick_params(bottom=False, top=False, left=False, right=False)
    ax.tick_params(
        labelbottom=False, labeltop=False, labelleft=False, labelright=False)

    thumbnail_ax = fig.add_axes((.03, .83, .15, .15))
    thumbnail_ax.imshow(image)
    thumbnail_ax.tick_params(bottom=False, top=False, left=False, right=False)
    thumbnail_ax.tick_params(
        labelbottom=False, labeltop=False, labelleft=False, labelright=False)

    fig.savefig(results_filename, dpi=300)
    plt.close()


def get_label(ax_ui, model, image, center, confidence):
    ax_ui.clear()
    ax_ui.imshow(image)
    draw_crosshairs(
        ax_ui,
        center,
        radius=20)
    ax_ui.set_xlim(-.5, image.shape[1] - .5)
    ax_ui.set_ylim(image.shape[0] - .5, -.5)
    ax_ui.set_title(
        f"{model.blocks['naive_bayes'].forward_out} " +
        f"conf {confidence}")
    ax_ui.set_xlabel("target: 0,   not target: 1", fontsize=8)
    plt.draw()
    plt.pause(0.01)

    keypress = sys.stdin.read(1)[0]
    print(keypress + "                                     ", end="\r")
    if keypress == "z":
        print("    Undo", end="\r")
        model.blocks["naive_bayes"].remove_last_observation()

        keypress = sys.stdin.read(1)[0]
        print(keypress + "                               ", end="\r")

    if keypress == "b":
        plt.close()
        plt.pause(0.01)
        print("\n    Moving on")
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, orig_settings)
        return model

    if keypress not in ["0", "1"]:
        return

    key_category = int(keypress)
    model.blocks["naive_bayes"].add_labeled_observation(key_category)
    return key_category


def draw_crosshairs(ax, center, radius=10):
    linewidth_white = 2
    linewidth_black = 1
    zorder = 5
    t = np.linspace(0, 2.01 * np.pi, 100)
    yc, xc = center
    x = np.cos(t) * radius + xc
    y = np.sin(t)* radius + yc
    ax.plot(x, y, color="white", linewidth=linewidth_white, zorder=zorder)
    ax.plot(
        [xc, xc],
        [yc + radius * .5, yc + radius * 1.5],
        color="white",
        linewidth=linewidth_white,
        solid_capstyle="round",
        zorder=zorder)
    ax.plot(
        [xc, xc],
        [yc - radius * .5, yc - radius * 1.5],
        color="white",
        linewidth=linewidth_white,
        solid_capstyle="round",
        zorder=zorder)
    ax.plot(
        [xc + radius * .5, xc + radius * 1.5],
        [yc, yc],
        color="white",
        linewidth=linewidth_white,
        solid_capstyle="round",
        zorder=zorder)
    ax.plot(
        [xc - radius * .5, xc - radius * 1.5],
        [yc, yc],
        color="white",
        linewidth=linewidth_white,
        solid_capstyle="round",
        zorder=zorder)

    ax.plot(x, y, color="black", linewidth=linewidth_black, zorder=zorder)
    ax.plot(
        [xc, xc],
        [yc + radius * .5, yc + radius * 1.5],
        color="black",
        linewidth=linewidth_black,
        solid_capstyle="round",
        zorder=zorder)
    ax.plot(
        [xc, xc],
        [yc - radius * .5, yc - radius * 1.5],
        color="black",
        linewidth=linewidth_black,
        solid_capstyle="round",
        zorder=zorder)
    ax.plot(
        [xc + radius * .5, xc + radius * 1.5],
        [yc, yc],
        color="black",
        linewidth=linewidth_black,
        solid_capstyle="round",
        zorder=zorder)
    ax.plot(
        [xc - radius * .5, xc - radius * 1.5],
        [yc, yc],
        color="black",
        linewidth=linewidth_black,
        solid_capstyle="round",
        zorder=zorder)


def show_performance(iterations, errors, training_figfile):
    fig = plt.figure()
    ax = fig.gca()
    ax.plot(iterations, errors, color="#04253a")
    ax.set_xlabel("Iteration")
    ax.set_ylabel("Training error")
    ax.set_ylim(0, 50)
    ax.grid()
    plt.savefig(training_figfile, dpi=300)
    plt.close()
