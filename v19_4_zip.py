from copy import deepcopy
import os
import numpy as np
from image_diff import ImageDiff
from cottonwood.operations import Copy, Flatten, Stack
from cottonwood.operations_2d import Crop
from cottonwood.structure import Structure
from cottonwood.experimental.ziptie_block import Ziptie
from cottonwood.pooling import AvgPool2D
from v10_data_tools import save_model, get_ziptie_patches

# n_bundles_str = "10"
# n_bundles_str = "50"
# n_bundles_str = "100"
# n_bundles_str = "200"
# n_bundles_str = "300"
n_bundles_str = "1000"
# n_bundles_str = "3000"

# threshold_str = "1"
threshold_str = "10"
# threshold_str = "100"
# threshold_str = "1000"

n_bundles = int(n_bundles_str)
threshold = int(threshold_str)

model_filename = f"model_{n_bundles_str}b_{threshold_str}t.pkl"
n_iter_update = int(1e2)


def main():
    model = build_unsupervised_model()
    train_unsupervised(model)
    save_model(model, model_filename)


def build_unsupervised_model():
    # Build a succession of zipties for each resolution.
    # This structure can be replicated for convenience.
    res_model = Structure()
    res_model.add(ImageDiff(), "diff")
    res_model.add(Flatten(), "flatten")
    res_model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_0")
    res_model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_1")
    res_model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_2")
    res_model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_3")
    res_model.add(Copy(), "copy_0")
    res_model.add(Copy(), "copy_1")
    res_model.add(Copy(), "copy_2")
    res_model.add(Stack(), "stack_0_1")
    res_model.add(Stack(), "stack_2_3")
    res_model.add(Stack(), "stack_0_3")

    res_model.connect("diff", "flatten")
    res_model.connect("flatten", "ziptie_0")
    res_model.connect("ziptie_0", "copy_0")
    res_model.connect("copy_0", "ziptie_1")
    res_model.connect("ziptie_1", "copy_1")
    res_model.connect("copy_1", "ziptie_2")
    res_model.connect("ziptie_2", "copy_2")
    res_model.connect("copy_2", "ziptie_3")
    res_model.connect("ziptie_3", "stack_2_3")
    res_model.connect("copy_2", "stack_2_3", i_port_tail=1, i_port_head=1)
    res_model.connect("copy_0", "stack_0_1", i_port_tail=1, i_port_head=1)
    res_model.connect("copy_1", "stack_0_1", i_port_tail=1, i_port_head=0)
    res_model.connect("stack_0_1", "stack_0_3", i_port_head=1)
    res_model.connect("stack_2_3", "stack_0_3")

    model = Structure()
    model.add(Copy(), "copy_res")
    model.add(Copy(), "copy_med_lo")
    model.add(Crop(n_crop=((44, 44), (44, 44))), "crop_hi")
    model.add(Crop(n_crop=((33, 33), (33, 33))), "crop_med")
    model.add(AvgPool2D(window=3, stride=3), "pool_med")
    model.add(AvgPool2D(window=9, stride=9), "pool_lo")
    model.add(deepcopy(res_model), "hi_res")
    model.add(deepcopy(res_model), "med_res")
    model.add(deepcopy(res_model), "lo_res")
    model.add(Stack(), "stack_med_lo")
    model.add(Stack(), "stack_res")

    model.connect("copy_res", "crop_hi")
    model.connect("crop_hi", "hi_res")
    model.connect("copy_res", "copy_med_lo", i_port_tail=1)
    model.connect("copy_med_lo", "crop_med", i_port_tail=0)
    model.connect("crop_med", "pool_med")
    model.connect("copy_med_lo", "pool_lo", i_port_tail=1)
    model.connect("pool_med", "med_res")
    model.connect("pool_lo", "lo_res")
    model.connect("hi_res", "stack_res")
    model.connect("med_res", "stack_med_lo")
    model.connect("lo_res", "stack_med_lo", i_port_head=1)
    model.connect("stack_med_lo", "stack_res", i_port_head=1)

    return model


def all_zipties_full(model):
    all_full = True
    if model.blocks["hi_res"].blocks["ziptie_3"].forward_out is None:
        all_full = False
    if model.blocks["med_res"].blocks["ziptie_3"].forward_out is None:
        all_full = False
    if model.blocks["lo_res"].blocks["ziptie_3"].forward_out is None:
        all_full = False
    return all_full


def train_unsupervised(model):
    i_iter = 0
    patch_generator = get_ziptie_patches()
    while not all_zipties_full(model):
        i_iter += 1
        patch, center = next(patch_generator)
        model.forward_pass(patch)
        if i_iter % n_iter_update == 0:
            try:
                print(
                    i_iter,
                    "hi 3",
                    model.blocks["hi_res"].blocks["ziptie_3"].algo.n_bundles,
                    "med 3",
                    model.blocks["med_res"].blocks["ziptie_3"].algo.n_bundles,
                    "lo 3",
                    model.blocks["lo_res"].blocks["ziptie_3"].algo.n_bundles,
                    )
            except Exception:
                try:
                    print(
                        i_iter,
                        "hi 2",
                        model.blocks["hi_res"].blocks["ziptie_2"].algo.n_bundles,
                        "med 2",
                        model.blocks["med_res"].blocks["ziptie_2"].algo.n_bundles,
                        "lo 2",
                        model.blocks["lo_res"].blocks["ziptie_2"].algo.n_bundles,
                        )
                except Exception:
                    try:
                        print(
                            i_iter,
                            "hi 1",
                            model.blocks["hi_res"].blocks["ziptie_1"].algo.n_bundles,
                            "med 1",
                            model.blocks["med_res"].blocks["ziptie_1"].algo.n_bundles,
                            "lo 1",
                            model.blocks["lo_res"].blocks["ziptie_1"].algo.n_bundles,
                            )

                    except Exception:
                        print(
                            i_iter,
                            "hi 0",
                            model.blocks["hi_res"].blocks["ziptie_0"].algo.n_bundles,
                            "med 0",
                            model.blocks["med_res"].blocks["ziptie_0"].algo.n_bundles,
                            "lo 0",
                            model.blocks["lo_res"].blocks["ziptie_0"].algo.n_bundles,
                            )

    return model


if __name__ == "__main__":
    main()
