import numpy as np
from image_diff import ImageDiff
from cottonwood.operations import Stack
from cottonwood.structure import Structure
from cottonwood.experimental.naive_bayes import NaiveBayes
from v10_data_tools import retrieve_model, get_training_patches
from v21_ux_tools_5_zip import PixelPicker

# n_bundles_str = "10"
# n_bundles_str = "50"
# n_bundles_str = "100"
# n_bundles_str = "200"
# n_bundles_str = "300"
# n_bundles_str = "1000"
n_bundles_str = "3000"

threshold_str = "1"
# threshold_str = "10"
# threshold_str = "100"
# threshold_str = "1000"

model_filename = f"model_{n_bundles_str}b_{threshold_str}t.pkl"
model_supervised_filename = (
    f"model_super_{n_bundles_str}b_{threshold_str}t.pkl")
training_figfile = "training.png"

n_iter_train = int(5e4)
n_iter_update = int(1e3)


def main():
    try:
        model = retrieve_model(model_supervised_filename)
    except Exception:
        model = retrieve_model(model_filename)
        model.add(NaiveBayes(n_categories=2), "naive_bayes")
        model.connect("stack_res", "naive_bayes")

    train_supervised(model)
    save_model(model, model_supervised_filename)


def train_supervised(model):
    n_labels = 0
    done = False
    i_iter = 0
    while not done:
        image, centers, patches = get_training_patches()
        confidences = []
        categories = []
        for i_patch in range(len(patches)):
            model.forward_pass(patches[i_patch])
            i_iter += 1
            if i_iter >= n_iter_train:
                done = True

            # If less confident, get a label
            category = float(model.blocks["naive_bayes"].forward_out)
            if category is None:
                continue
            categories.append(category)
            confidences.append(float(model.blocks["naive_bayes"].confidence))

        labeler = PixelPicker(image, model, categories, centers, confidences)
        n_new_labels = labeler.get_labels()

        # Increment n_labels
        n_labels += n_new_labels
        print(f"{n_labels} labels so far                           ", end="\r")

    return model


if __name__ == "__main__":
    main()
