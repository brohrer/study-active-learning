import os
import numpy as np
from image_diff import ImageDiff
from cottonwood.operations import Copy, Flatten
from cottonwood.structure import Structure
from cottonwood.experimental.ziptie_block import Ziptie
from cottonwood.pooling import AvgPool2D
from v10_data_tools import save_model, get_ziptie_patches

# n_bundles_str = "10"
n_bundles_str = "50"
# n_bundles_str = "100"
# n_bundles_str = "200"
# n_bundles_str = "300"
# n_bundles_str = "1000"

threshold_str = "1"
# threshold_str = "10"
# threshold_str = "100"
# threshold_str = "1000"

n_d_bundles = int(n_bundles_str)
threshold = int(threshold_str)

model_filename = f"model_{n_bundles_str}b_{threshold_str}t.pkl"
n_iter_update = int(1e3)


def main():
    model = initialize_model()
    train_unsupervised(model)
    save_model(model, model_filename)


def initialize_model():
    model = Structure()
    model.add(ImageDiff(), "diff")
    model.add(AvgPool2D(window=2, stride=2), "pool_d")
    model.add(Flatten(), "flatten_d")
    model.add(Ziptie(n_outputs=n_d_bundles, threshold=threshold), "ziptie_d_0")

    model.connect("diff", "pool_d")
    model.connect("pool_d", "flatten_d")
    model.connect("flatten_d", "ziptie_d_0")
    return model


def train_unsupervised(model):
    i_iter = 0
    patch_generator = get_ziptie_patches()
    while not model.blocks["ziptie_d_0"].is_full():
        i_iter += 1
        patch, center = next(patch_generator)
        model.forward_pass(patch)
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_d_0"].algo.n_bundles,
                "z0 bundles at iter",
                i_iter)

    model.add(Copy(), "copy_d_0")
    model.add(Ziptie(n_outputs=n_d_bundles, threshold=threshold), "ziptie_d_1")
    model.connect("ziptie_d_0", "copy_d_0")
    model.connect("copy_d_0", "ziptie_d_1")

    while not model.blocks["ziptie_d_1"].is_full():
        i_iter += 1
        patch, center = next(patch_generator)
        model.forward_pass(patch)
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_d_1"].algo.n_bundles,
                "z1 bundles at iter",
                i_iter)

    model.add(Copy(), "copy_d_1")
    model.add(Ziptie(n_outputs=n_d_bundles, threshold=threshold), "ziptie_d_2")
    model.connect("ziptie_d_1", "copy_d_1")
    model.connect("copy_d_1", "ziptie_d_2")

    while not model.blocks["ziptie_d_2"].is_full():
        i_iter += 1
        patch, center = next(patch_generator)
        model.forward_pass(patch)
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_d_2"].algo.n_bundles,
                "z2 bundles at iter",
                i_iter)

    model.add(Copy(), "copy_d_2")
    model.add(Ziptie(n_outputs=n_d_bundles, threshold=threshold), "ziptie_d_3")
    model.connect("ziptie_d_2", "copy_d_2")
    model.connect("copy_d_2", "ziptie_d_3")

    while not model.blocks["ziptie_d_3"].is_full():
        i_iter += 1
        patch, center = next(patch_generator)
        model.forward_pass(patch)
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_d_3"].algo.n_bundles,
                "z3 bundles at iter",
                i_iter)
    return model


if __name__ == "__main__":
    main()
