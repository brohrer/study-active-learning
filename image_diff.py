import numpy as np


class ImageDiff:
    def __init__(self):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return("Vertical and horizonal one-pixel differences")

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        self.n_rows, self.n_cols, self.n_channels = self.forward_in.shape
        self.forward_out = np.zeros((
            self.n_rows - 2, self.n_cols - 2, self.n_channels * 4))

        for i_channel in range(self.n_channels):
            # Vertical difference
            i_vert_channel_pos = i_channel * 4
            i_vert_channel_neg = i_channel * 4 + 1
            vert_diff = (
                self.forward_in[2:, 1:-1, i_channel] -
                self.forward_in[0:-2, 1:-1, i_channel])
            i_pos = np.where(vert_diff > 0)
            i_neg = np.where(vert_diff < 0)
            self.forward_out[
                i_pos[0], i_pos[1], i_vert_channel_pos] = vert_diff[i_pos]
            self.forward_out[
                i_neg[0], i_neg[1], i_vert_channel_neg] = -vert_diff[i_neg]

            # Horizontal difference
            i_horz_channel_pos = i_channel * 4 + 2
            i_horz_channel_neg = i_channel * 4 + 3
            horz_diff = (
                self.forward_in[1:-1, 2:, i_channel] -
                self.forward_in[1:-1, 0:-2, i_channel])
            i_pos = np.where(horz_diff > 0)
            i_neg = np.where(horz_diff < 0)
            self.forward_out[
                i_pos[0], i_pos[1], i_horz_channel_pos] = horz_diff[i_pos]
            self.forward_out[
                i_neg[0], i_neg[1], i_horz_channel_neg] = -horz_diff[i_neg]

        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        n_channels_in = self.backward_in.shape[2]
        self.backward_out = np.zeros((
            self.n_rows, self.n_cols, self.n_channels))
        for i_channel in range(n_channels_in // 4):
            vert_diff = (
                self.backward_in[:, :, 4 * i_channel] -
                self.backward_in[:, :, 4 * i_channel + 1])
            self.backward_out[2:, 1:-1, i_channel] += vert_diff
            self.backward_out[0:-2, 1:-1, i_channel] -= vert_diff

            horz_diff = (
                self.backward_in[:, :, 4 * i_channel + 2] -
                self.backward_in[:, :, 4 * i_channel + 3])
            self.backward_out[1:-1, 2:, i_channel] += horz_diff
            self.backward_out[1:-1, 0:-2, i_channel] -= horz_diff

        return self.backward_out
