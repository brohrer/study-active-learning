import os
import pickle as pkl
import sys
import termios
import tty
import numpy as np
import matplotlib.pyplot as plt
from conv2d_edges_horz_vert import Conv2DEdgesHorzVert as Conv2DEdges
# from conv2d_edges import Conv2DEdges
from cottonwood.operations import Copy, Flatten, Stack
from cottonwood.structure import Structure
from cottonwood.experimental.ziptie_block import Ziptie
from cottonwood.experimental.naive_bayes import NaiveBayes
from cottonwood.pooling import AvgPool2D
from imagenette_patches import TrainingData, TestingData

n_bundles_str = "100"
# n_bundles_str = "200"
# n_bundles_str = "300"
# n_bundles_str = "1000"

threshold_str = "10"
# threshold_str = "100"
# threshold_str = "1000"

n_d_bundles = int(n_bundles_str)
n_s_bundles = int(n_bundles_str)
threshold = int(threshold_str)

model_filename = f"model_imagenette_{n_bundles_str}b_{threshold_str}t.pkl"
model_supervised_filename = (
    f"model_imagenette_super_{n_bundles_str}b_{threshold_str}t.pkl")

training_figfile = "training.png"

n_iter_train = int(5e4)
n_iter_test = int(1e4)
n_iter_update = int(1e3)


def main():
    if os.path.exists(model_filename):
        model = retrieve_model(model_filename)
    else:
        model = initialize_model()
        train_unsupervised(model)
        save_model(model, model_filename)

    train_supervised(model)
    test_supervised(model)
    save_model(model, model_supervised_filename)


def retrieve_model(filename):
    with open(filename, "rb") as f:
        model = pkl.load(f)
    model.add(TrainingData(), "training_data")
    model.connect("training_data", "conv")
    return model


def save_model(model, filename):
    model.remove("training_data")
    with open(filename, "wb") as f:
        pkl.dump(model, f)


def initialize_model():
    model = Structure()
    model.add(TrainingData(), "training_data")
    model.add(Conv2DEdges(kernel_size=3, n_kernels=2), "conv")
    model.add(AvgPool2D(window=2, stride=2), "pool_d")
    model.add(Flatten(), "flatten_d")
    model.add(Ziptie(n_outputs=n_d_bundles, threshold=threshold), "ziptie_d_0")

    model.connect("training_data", "conv")
    model.connect("conv", "pool_d")
    model.connect("pool_d", "flatten_d")
    model.connect("flatten_d", "ziptie_d_0")
    return model


def train_unsupervised(model):
    i_iter = 0
    while not model.blocks["ziptie_d_0"].is_full():
        i_iter += 1
        model.forward_pass()
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_d_0"].algo.n_bundles,
                "z0 bundles deriv at iter",
                i_iter)

    model.add(Copy(), "copy_d_0")
    model.add(Ziptie(n_outputs=n_d_bundles, threshold=threshold), "ziptie_d_1")
    model.connect("ziptie_d_0", "copy_d_0")
    model.connect("copy_d_0", "ziptie_d_1")

    while not model.blocks["ziptie_d_1"].is_full():
        i_iter += 1
        model.forward_pass()
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_d_1"].algo.n_bundles,
                "z1 bundles deriv at iter",
                i_iter)

    model.add(Copy(), "copy_d_1")
    model.add(Ziptie(n_outputs=n_d_bundles, threshold=threshold), "ziptie_d_2")
    model.connect("ziptie_d_1", "copy_d_1")
    model.connect("copy_d_1", "ziptie_d_2")

    while not model.blocks["ziptie_d_2"].is_full():
        i_iter += 1
        model.forward_pass()
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_d_2"].algo.n_bundles,
                "z2 bundles deriv at iter",
                i_iter)

    model.add(Copy(), "copy_d_2")
    model.add(Ziptie(n_outputs=n_d_bundles, threshold=threshold), "ziptie_d_3")
    model.connect("ziptie_d_2", "copy_d_2")
    model.connect("copy_d_2", "ziptie_d_3")

    while not model.blocks["ziptie_d_3"].is_full():
        i_iter += 1
        model.forward_pass()
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_d_3"].algo.n_bundles,
                "z3 bundles deriv at iter",
                i_iter)
    return model


def train_supervised(model):

    # Set up to grab keypresses
    orig_settings = termios.tcgetattr(sys.stdin)
    tty.setcbreak(sys.stdin)
    key = 0

    # Set up example image
    fig_ui = plt.figure()
    plt.ion()
    plt.show()
    ax_ui = fig_ui.gca()
    ax_ui.imshow(np.zeros((5, 5)))
    plt.draw()
    plt.pause(0.001)

    model.add(TrainingData(), "training_data")
    model.add(Stack(), "stack_d_0_1")
    model.add(Stack(), "stack_d_2_3")
    model.add(Stack(), "stack_d_all")
    model.add(NaiveBayes(n_categories=10), "naive_bayes")
    model.connect("training_data", "conv")
    # model.connect("training_data", "naive_bayes", i_port_tail=1, i_port_head=1)

    model.connect("copy_d_0", "stack_d_0_1", i_port_head=0, i_port_tail=1)
    model.connect("copy_d_1", "stack_d_0_1", i_port_head=1, i_port_tail=1)
    model.connect("copy_d_2", "stack_d_2_3", i_port_head=0, i_port_tail=1)
    model.connect("ziptie_d_3", "stack_d_2_3", i_port_head=1, i_port_tail=0)
    model.connect("stack_d_0_1", "stack_d_all", i_port_head=0, i_port_tail=0)
    model.connect("stack_d_2_3", "stack_d_all", i_port_head=1, i_port_tail=0)
    model.connect("stack_d_all", "naive_bayes")

    label_names = model.blocks["training_data"].label_names
    label_indices = {}
    for i_label, label in enumerate(label_names):
        label_indices[label] = int(i_label)
    print(label_indices)

    misses = []
    accuracies = []
    n_labels = 0
    for i_iter in range(n_iter_train):
        model.forward_pass()

        # If less confident, get a label
        confidence = float(model.blocks["naive_bayes"].confidence)
        confidence_buffer = 1 / (n_labels + 1)
        if np.random.sample() < 2 ** -confidence + confidence_buffer:
            ax_ui.imshow(model.blocks["training_data"].forward_out[0])
            truth_category = label_indices[
                model.blocks["training_data"].forward_out[1]]
            ax_ui.set_title(
                f"{truth_category} " +
                f"{model.blocks['naive_bayes'].forward_out} " +
                f"conf {confidence}")
            plt.draw()
            plt.pause(0.01)

            # print("Which class?")
            keypress = sys.stdin.read(1)[0]
            print(keypress)
            if keypress == "z":
                print("    Undo")
                model.blocks["naive_bayes"].remove_last_observation()
                continue

            if keypress == "b":
                print("    Moving on")
                termios.tcsetattr(sys.stdin, termios.TCSADRAIN, orig_settings)
                return model

            if keypress not in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]:
                continue

            key_category = int(keypress)
            n_labels += 1
            model.blocks["naive_bayes"].add_labeled_observation(key_category)

            try:
                if (key_category == model.blocks["naive_bayes"].forward_out):
                    misses.append(0)
                else:
                    misses.append(1)
            except TypeError:
                # Initially some of the estimates are None. Ignore these
                pass

        if (i_iter + 1) % n_iter_update == 0:

            recent_misses = np.array(misses[-n_iter_update:])
            accuracy = 1 - np.mean(recent_misses)
            accuracies.append(accuracy)
            print(
                f"{np.sum(recent_misses)} out of " +
                f"{recent_misses.size} digits misclassified " +
                f"at iteration {i_iter + 1} " +
                f"with {n_labels} labels " +
                f"for an accuracy of {accuracy * 100:.04} percent.")
            iterations = (np.arange(len(accuracies)) + 1) * n_iter_update
            errors = (1 - np.array(accuracies)) * 100

            fig = plt.figure()
            ax = fig.gca()
            ax.plot(iterations, errors, color="#04253a")
            ax.set_title("Naive Bayes with MNIST digits")
            ax.set_xlabel("Iteration")
            ax.set_ylabel("Training error")
            ax.set_ylim(0, 50)
            ax.grid()
            plt.savefig(training_figfile, dpi=300)
            plt.close()

            # show_bundles(
            #     model.blocks["ziptie_0"].algo,
            #     model.blocks["ziptie_1"].algo,
            #     model.blocks["ziptie_2"].algo,
            #     model.blocks["ziptie_3"].algo)

    # Undo keypress grabbing
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, orig_settings)

    return model


def test_supervised(model):
    model.add(TestingData(), "testing_data")
    model.connect("testing_data", "conv")
    # Uncomment this line to train on the testing data too
    # model.connect("testing_data", "naive_bayes", i_port_tail=1, i_port_head=1)

    label_names = model.blocks["testing_data"].label_names
    label_indices = {}
    for i_label, label in enumerate(label_names):
        label_indices[label] = int(i_label)

    misses = []
    accuracies = []
    for i_iter in range(n_iter_test):
        model.forward_pass()
        truth_category = label_indices[
            model.blocks["training_data"].forward_out[1]]
        try:
            if truth_category == model.blocks["naive_bayes"].forward_out:
                misses.append(0)
            else:
                misses.append(1)
        except TypeError:
            # Initially some of the estimates are None. Ignore these
            pass

    misses = np.array(misses)
    accuracy = 1 - np.mean(misses)
    accuracies.append(accuracy)
    print("Testing")
    print(
        f"{np.sum(misses)} out of " +
        f"{misses.size} digits misclassified " +
        f"for an accuracy of {accuracy * 100:.04} percent.")
    return model


'''
def show_bundles(zt0, zt1, zt2, zt3):

    if zt3.n_bundles == 0:
        return
    # Select some bundles to visualize.
    n_bundle_viz = np.minimum(10, zt3.n_bundles)
    i_bundles = np.random.choice(
        zt3.n_bundles, size=n_bundle_viz, replace=False)
    for i_bundle in i_bundles:

        # Create a single active bundle signal.
        bundle_activities_3 = np.zeros(zt3.n_bundles)
        bundle_activities_3[int(i_bundle)] = 1

        # Get the corresponding cable (input) activities.
        bundle_activities_2 = zt3.project_bundle_activities(bundle_activities_3)
        bundle_activities_1 = zt2.project_bundle_activities(bundle_activities_2)
        bundle_activities_0 = zt1.project_bundle_activities(bundle_activities_1)
        activities = zt0.project_bundle_activities(bundle_activities_0)
        img_size = int(np.sqrt(activities.size))
        img = np.reshape(activities, (img_size, img_size))

        fig = plt.figure()
        ax = fig.gca()
        ax.imshow(img, cmap="gray")
        plt.savefig(os.path.join("images", f"bundle_{i_bundle}.png"))
        plt.close()
'''


if __name__ == "__main__":
    main()
