import os
import pickle as pkl
from PIL import Image
import numpy as np
from cottonwood.pooling import avg_pool_2d

patch_size = 33 * 3
patches_per_image = 13
spacing = 30
jitter = spacing // 4

image_dir = "pics"
images = []


def load_images():
    filenames = os.listdir(image_dir)
    np.random.shuffle(filenames)
    for filename in filenames:
        filename_postfix = filename.split(".")[-1]
        if filename_postfix.lower() in ["png", "jpg", "jpeg"]:
            unsized_image = np.asarray(
                Image.open(os.path.join(image_dir, filename)))
            image = resize_image(unsized_image) / 256
            if image.shape[2] > 3:
                image = image[:, :, :3]
            images.append(image)


def resize_image(unsized):
    # Make the largest dimension as close as it can get to a target size
    target_size = 500
    largest_dim = np.maximum(unsized.shape[0], unsized.shape[1])
    size_factor = int(np.ceil(largest_dim / target_size))
    n_pooled_rows = unsized.shape[0] // size_factor
    n_pooled_cols = unsized.shape[1] // size_factor
    image = avg_pool_2d(
        unsized, size_factor, size_factor, n_pooled_rows, n_pooled_cols)
    return image


def retrieve_model(filename):
    with open(filename, "rb") as f:
        model = pkl.load(f)
    return model


def save_model(model, filename):
    with open(filename, "wb") as f:
        pkl.dump(model, f)


def get_ziptie_patches():
    image = images[np.random.randint(len(images))]
    while True:
        if np.random.sample() < 1 / patches_per_image:
            image = images[np.random.randint(len(images))]

        n_rows, n_cols, _ = image.shape
        i_row = np.random.randint(n_rows - patch_size)
        i_col = np.random.randint(n_cols - patch_size)
        patch = image[
            i_row: i_row + patch_size,
            i_col: i_col + patch_size, :]
        patch_center = (i_row + patch_size // 2, i_col + patch_size // 2)
        yield patch, patch_center


def get_training_patches():
    image = images[np.random.randint(len(images))]
    n_rows, n_cols, _ = image.shape
    centers = []
    patches = []

    # Get a regular grid of patches.
    i_row_min = 1
    i_col_min = 1
    i_row_max = n_rows - patch_size
    i_col_max = n_cols - patch_size

    # spacing = int(np.sqrt(i_row_max * i_col_max / n_training_patches))
    patch_rows = np.arange(0, i_row_max, spacing)
    patch_cols = np.arange(0, i_col_max, spacing)
    for i_row in patch_rows:
        for i_col in patch_cols:
            i_row_jitter = (np.minimum(i_row_max - 1, np.maximum(i_row_min,
                    i_row + np.random.randint(2 * jitter) - jitter))
                    + patch_size // 2)
            i_col_jitter = (np.minimum(i_col_max - 1, np.maximum(i_col_min,
                    i_col + np.random.randint(2 * jitter) - jitter))
                    + patch_size // 2)
            patch_center = (i_row_jitter, i_col_jitter)
            patch = get_patch(image, patch_center)
            patches.append(patch)
            centers.append(patch_center)

    return image, centers, patches


def get_patch(image, center):
    i_row_center, i_col_center = center
    i_row = i_row_center - patch_size // 2
    i_col = i_col_center - patch_size // 2
    patch = image[
        i_row: i_row + patch_size,
        i_col: i_col + patch_size, :]
    return patch


load_images()
print(f"{len(images)} images loaded")
