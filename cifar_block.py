import os
import pickle
import numpy as np


N_IMG_ROWS = 32
N_IMG_COLS = 32
N_CHANNELS = 3
data_shape = (N_IMG_ROWS, N_IMG_COLS, N_CHANNELS)
DATA_DIR = "cifar-10-batches-py"
TRAINING_DATA_FILES = [
    "data_batch_1",
    "data_batch_2",
    "data_batch_3",
    "data_batch_4",
    "data_batch_5",
]
TEST_DATA_FILE = "test_batch"
METADATA_FILE = "batches.meta"


def load_data(filename):
    with open(filename, 'rb') as fo:
        contents = pickle.load(fo, encoding='bytes')
    return contents


class TrainingData(object):
    def __init__(self):
        metadata = load_data(os.path.join(DATA_DIR, METADATA_FILE))
        self.label_names = metadata[b"label_names"]
        self.counter = 0
        self.i_data_file = None
        self.load_next_data_file()
        self.image = None
        self.label_name = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def load_next_data_file(self):
        if self.i_data_file is None:
            self.i_data_file = -1
        self.i_data_file += 1
        if self.i_data_file >= len(TRAINING_DATA_FILES):
            self.i_data_file = 0

        data_and_labels = load_data(
            os.path.join(DATA_DIR, TRAINING_DATA_FILES[self.i_data_file]))
        self.data = data_and_labels[b"data"]
        self.labels = data_and_labels[b"labels"]
        self.indices = np.arange(len(self.labels), dtype=int)
        # Reshuffle the training images after each epoch -
        # each complete pass through the training set
        np.random.shuffle(self.indices)

    def __str__(self):
        return "CIFAR training data"

    def forward_pass(self, forward_in):
        self.counter += 1
        if self.counter >= len(self.labels):
            self.load_next_data_file()
            self.counter = 0

        i_sample = self.indices[self.counter]

        # This is some fancy footwork to get the data in the right shape.
        # Cottonwood is going to expect the data to be in a
        #    rows x columns x channels
        # format. It is in a
        #    channels x rows x columns
        # format initially.
        self.image = np.reshape(
            self.data[i_sample, :],
            (N_CHANNELS, N_IMG_ROWS, N_IMG_COLS)).transpose(1, 2, 0)
        self.image = self.image / 256
        # The label names are initially byte-encoded.
        # Decode to make them plain old strings.
        self.label_name = self.label_names[
            self.labels[i_sample]].decode(encoding='UTF-8')
        self.forward_out = (self.image, self.label_name)
        return self.forward_out

    def backward_pass(self, backward_in):
        return self.backward_out


class TestingData(object):
    def __init__(self):
        metadata = load_data(os.path.join(DATA_DIR, METADATA_FILE))
        self.label_names = metadata[b"label_names"]
        data_and_labels = load_data(
            os.path.join(DATA_DIR, TEST_DATA_FILE))
        self.data = data_and_labels[b"data"]
        self.labels = data_and_labels[b"labels"]
        self.indices = np.arange(len(self.labels), dtype=int)
        np.random.shuffle(self.indices)
        self.counter = 0
        # self.shape = (N_IMG_ROWS, N_IMG_COLS, N_CHANNELS)
        self.image = None
        self.label_name = None

        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

    def __str__(self):
        return "CIFAR testing data"

    def forward_pass(self, foward_in):
        self.counter += 1
        if self.counter >= len(self.labels):
            self.counter = 0

        i_sample = self.indices[self.counter]
        self.image = np.reshape(
            self.data[i_sample, :],
            (N_CHANNELS, N_IMG_ROWS, N_IMG_COLS)).transpose(1, 2, 0)
        self.image = self.image / 256
        self.label_name = self.label_names[
            self.labels[i_sample]].decode(encoding='UTF-8')
        self.forward_out = (self.image, self.label_name)
        return self.forward_out

    def backward_pass(self, backward_in):
        return self.backward_out


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    data_block = TrainingData()
    for i in range(10):
        image, label_name = data_block.forward_pass(None)
        print(label_name)
        plt.imshow(image)
        plt.savefig(f"cifar_10_train_{i}.png")
        plt.close()

    data_block = TestingData()
    for i in range(10):
        image, label_name = data_block.forward_pass(None)
        print(label_name)
        plt.imshow(image)
        plt.savefig(f"cifar_10_test_{i}.png")
        plt.close()
