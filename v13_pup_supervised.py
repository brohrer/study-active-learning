import numpy as np
from image_diff import ImageDiff
from cottonwood.operations import Stack
from cottonwood.structure import Structure
from cottonwood.experimental.naive_bayes import NaiveBayes
from v10_data_tools import retrieve_model, get_training_patches
from v12_ux_tools import (
    clear_labeling_figure,
    generate_report,
    get_label,
    initiate_keypress_grabbing,
    initialize_labeling_figure,
    show_performance,
    show_result,
    terminate_keypress_grabbing)

# n_bundles_str = "10"
# n_bundles_str = "50"
# n_bundles_str = "100"
# n_bundles_str = "200"
n_bundles_str = "300"
# n_bundles_str = "1000"

# threshold_str = "1"
threshold_str = "10"
# threshold_str = "100"
# threshold_str = "1000"

model_filename = f"model_{n_bundles_str}b_{threshold_str}t.pkl"
model_supervised_filename = (
    f"model_super_{n_bundles_str}b_{threshold_str}t.pkl")
training_figfile = "training.png"

n_iter_train = int(5e4)
n_iter_update = int(1e3)
n_labels_per_image = 23


def main():
    model = retrieve_model(model_filename)
    train_supervised(model)
    save_model(model, model_supervised_filename)


def train_supervised(model):
    initiate_keypress_grabbing()
    key = 0
    ax_ui = initialize_labeling_figure()

    model.add(NaiveBayes(n_categories=2), "naive_bayes")
    model.connect("stack_res", "naive_bayes")

    misses = []
    accuracies = []
    n_labels = 0
    done = False
    i_iter = 0
    while not done:
        image, centers, patches = get_training_patches()
        confidences = []
        categories = []
        for i_patch in range(len(patches)):
            model.forward_pass(patches[i_patch])
            i_iter += 1
            if i_iter >= n_iter_train:
                done = True

            # If less confident, get a label
            category = float(model.blocks["naive_bayes"].forward_out)
            if category is None:
                continue
            categories.append(category)
            confidences.append(float(model.blocks["naive_bayes"].confidence))

            if (i_iter + 1) % n_iter_update == 0:

                recent_misses = np.array(misses[-n_iter_update:])
                accuracy = 1 - np.mean(recent_misses)
                accuracies.append(accuracy)
                print(
                    f"{np.sum(recent_misses)} out of " +
                    f"{recent_misses.size} digits misclassified " +
                    f"at iteration {i_iter + 1} " +
                    f"with {n_labels} labels " +
                    f"for an accuracy of {accuracy * 100:.04} percent.")
                iterations = (np.arange(len(accuracies)) + 1) * n_iter_update
                errors = (1 - np.array(accuracies)) * 100
                show_performance(iterations, errors, training_figfile)

        # Show the results
        show_result(image, centers, categories, confidences)

        # Choose less confident patches to label
        n_to_label = np.minimum(len(confidences), n_labels_per_image - 1)
        if n_to_label < 1:
            continue

        print("1 for hit, 0 for miss                           ", end="\r")
        i_partitioned = np.argpartition(
            np.array(confidences), n_to_label)
        i_to_label = i_partitioned[:n_to_label]
        for i in i_to_label:
            label_category = get_label(
                ax_ui, model, image, centers[i], confidences[i])
            n_labels += 1

            try:
                if (label_category == model.blocks["naive_bayes"].forward_out):
                    misses.append(0)
                else:
                    misses.append(1)
            except TypeError:
                # Initially some of the estimates are None. Ignore these
                pass

        # Clear UI image
        clear_labeling_figure(ax_ui)
        print("Wait a 10 seconds to process the next image.", end="\r")

    terminate_keypress_grabbing()
    return model


if __name__ == "__main__":
    main()
