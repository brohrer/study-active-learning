from copy import deepcopy
import os
import numpy as np
from image_diff import ImageDiff
from cottonwood.operations import Copy, Flatten, Stack
from cottonwood.operations_2d import Crop
from cottonwood.structure import Structure
from cottonwood.experimental.ziptie_block import Ziptie
from cottonwood.pooling import AvgPool2D
from v10_data_tools import save_model, get_ziptie_patches

# n_bundles_str = "10"
# n_bundles_str = "50"
n_bundles_str = "100"
# n_bundles_str = "200"
# n_bundles_str = "300"
# n_bundles_str = "1000"

threshold_str = "1"
# threshold_str = "10"
# threshold_str = "100"
# threshold_str = "1000"

n_bundles = int(n_bundles_str)
threshold = int(threshold_str)

model_filename = f"model_{n_bundles_str}b_{threshold_str}t.pkl"
n_iter_update = int(1e3)


def main():
    model = build_unsupervised_model()
    train_unsupervised(model)
    save_model(model, model_filename)


def build_unsupervised_model():
    # Build a succession of zipties for each resolution.
    # This structure can be replicated for convenience.
    res_model = Structure()
    res_model.add(ImageDiff(), "diff")
    res_model.add(Flatten(), "flatten")
    res_model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_0")
    res_model.add(Copy(), "copy_0")
    res_model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_1")
    res_model.add(Stack(), "stack_0_1")

    res_model.connect("diff", "flatten")
    res_model.connect("flatten", "ziptie_0")
    res_model.connect("ziptie_0", "copy_0")
    res_model.connect("copy_0", "stack_0_1")
    res_model.connect("copy_0", "ziptie_1", i_port_tail=1)
    res_model.connect("ziptie_1", "stack_0_1", i_port_head=1)

    model = Structure()
    model.add(Copy(), "res_copy")
    model.add(Crop(n_crop=((11, 11), (11, 11))), "crop_hi")
    model.add(AvgPool2D(window=3, stride=3), "pool_1")
    model.add(deepcopy(res_model), "hi_res")
    model.add(deepcopy(res_model), "med_res")
    model.add(Stack(), "res_stack")

    model.connect("res_copy", "crop_hi")
    model.connect("crop_hi", "hi_res")
    model.connect("res_copy", "pool_1", i_port_tail=1)
    model.connect("pool_1", "med_res")
    model.connect("hi_res", "res_stack")
    model.connect("med_res", "res_stack", i_port_head=1)

    return model


def all_zipties_full(model):
    all_full = True
    if model.blocks["hi_res"].blocks["ziptie_1"].forward_out is None:
        all_full = False
    if model.blocks["med_res"].blocks["ziptie_1"].forward_out is None:
        all_full = False
    return all_full


def train_unsupervised(model):
    i_iter = 0
    patch_generator = get_ziptie_patches()
    while not all_zipties_full(model):
        i_iter += 1
        patch, center = next(patch_generator)
        model.forward_pass(patch)
        if i_iter % n_iter_update == 0:
            print(
                i_iter,
                "hi",
                model.blocks["hi_res"].blocks["ziptie_0"].algo.n_bundles,
                model.blocks["hi_res"].blocks["ziptie_1"].algo.n_bundles,
                "med",
                model.blocks["med_res"].blocks["ziptie_0"].algo.n_bundles,
                model.blocks["med_res"].blocks["ziptie_1"].algo.n_bundles,
                )

    return model


if __name__ == "__main__":
    main()
