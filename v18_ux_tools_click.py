import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from v10_data_tools import get_patch, retrieve_model

# n_bundles_str = "10"
# n_bundles_str = "50"
# n_bundles_str = "100"
# n_bundles_str = "200"
n_bundles_str = "300"
# n_bundles_str = "1000"
# n_bundles_str = "3000"

threshold_str = "1"
# threshold_str = "10"
# threshold_str = "100"
# threshold_str = "1000"

model_filename = f"model_{n_bundles_str}b_{threshold_str}t.pkl"
figure_filename = f"model_{n_bundles_str}b_{threshold_str}t_features.png"


class PixelPicker:
    def __init__(self, image, model, categories, centers, confidences):
        self.categories = categories
        self.centers = centers
        self.confidences = confidences
        self.image = image
        self.model = model
        self.radii = [5, 7, 9, 11]

        self.cid = self.initialize()
        self.show_results()

    def initialize(self):
        self.fig = plt.figure()
        self.ax = self.fig.add_axes((.1, .1, .8, .8))
        # self.ax.imshow(np.zeros((100, 100)))
        self.ax.tick_params(bottom=False, top=False, left=False, right=False)
        self.ax.tick_params(
            labelbottom=False,
            labeltop=False,
            labelleft=False,
            labelright=False)

        cid = self.fig.canvas.mpl_connect(
            'button_press_event', self.onclick)

        self.thumbnail_ax = self.fig.add_axes((.01, .83, .15, .15))
        # self.thumbnail_ax.imshow(np.zeros((100, 100)))
        self.thumbnail_ax.tick_params(
            bottom=False, top=False, left=False, right=False)
        self.thumbnail_ax.tick_params(
            labelbottom=False,
            labeltop=False,
            labelleft=False,
            labelright=False)
        return cid

    def show_results(self):
        self.ax.clear()
        self.ax.imshow(self.image, origin="lower", alpha=.8)
        print("cats", self.categories)
        print("conf", self.confidences)
        for i in range(len(self.centers)):
            center = self.centers[i]
            category = self.categories[i]
            confidence = self.confidences[i]

            if category == 0:
                color = "black"
            elif category == 1:
                color = "white"
            else:
                color = "red"

            x = center[1]
            y = center[0]
            t = np.linspace(0, 2 * np.pi, 50)
            for r in self.radii:
                xs = x + r * np.cos(t)
                ys = y + r * np.sin(t)
                path = np.concatenate(
                    (xs[:, np.newaxis], ys[:, np.newaxis]),
                    axis=1)
                self.ax.add_patch(patches.Polygon(
                    path,
                    alpha=.2 * confidence,
                    edgecolor="none",
                    facecolor=color,
                    zorder=3,
                ))

        centers_array = np.array(self.centers)
        self.row_min = np.min(centers_array[:, 0])
        self.row_max = np.max(centers_array[:, 0])
        self.col_min = np.min(centers_array[:, 1])
        self.col_max = np.max(centers_array[:, 1])
        self.ax.set_xlim(self.col_min - .5, self.col_max + .5)
        self.ax.set_ylim(self.row_max + .5, self.row_min - .5)
        self.ax.set_xlabel(
            "Click a few places where there's a pup.\n" +
            "Click outside the image when you're done.", fontsize=12)

        self.thumbnail_ax.clear()
        self.thumbnail_ax.imshow(self.image, origin="lower")
        self.thumbnail_ax.set_xlim(self.col_min - .5, self.col_max + .5)
        self.thumbnail_ax.set_ylim(self.row_max + .5, self.row_min - .5)

        self.fig.canvas.mpl_connect("button_press_event", self.onclick)

    def onclick(self, event):
        x = event.xdata
        y = event.ydata
        clicked_outside = False
        print(x, y, "                                  ",  end="\r")
        if x is None or y is None:
            clicked_outside = True

        if clicked_outside:
            if self.finding_pups:
                self.finding_pups = False
                self.finding_nonpups = True
                self.ax.set_xlabel(
                    "Click a few places where there's NOT a pup.\n" +
                    "Click outside the image when you're done.", fontsize=12)
                self.fig.canvas.draw()
                return
            else:
                self.finding_nonpups = False
                self.done_finding = True

        if self.done_finding:
            self.fig.canvas.mpl_disconnect(self.cid)
            plt.close()
            return

        if self.finding_pups:
            category = 1
        else:
            category = 0

        center = [int(np.round(y)), int(np.round(x))]
        patch = get_patch(self.image, center)
        self.model.forward_pass(patch)
        self.model.blocks["naive_bayes"].add_labeled_observation(category)
        self.n_new_labels += 1

    def get_labels(self):
        # Track the state
        self.finding_pups = True
        self.finding_nonpups = False
        self.done_finding = False
        self.n_new_labels = 0

        plt.show()
        return self.n_new_labels


def sample_features(model_filename):
    model = retrieve_model(model_filename)
    image_path = "reports"
    os.makedirs(image_path, exist_ok=True)

    fig = plt.figure(figsize=(16, 12))
    n_resolutions = 3
    n_zipties_per_res = 4
    n_feature_rows = 5
    n_feature_cols = 5
    res_models = [
        model.blocks["hi_res"],
        model.blocks["med_res"],
        model.blocks["lo_res"],
    ]

    for i_res in range(n_resolutions):
        res_model = res_models[i_res]
        res_model.remove("stack_0_3")
        res_model.remove("stack_0_1")
        res_model.remove("stack_2_3")
        res_model.remove("copy_2")
        res_model.remove("copy_1")
        res_model.remove("copy_0")
        res_model.connect("ziptie_0", "ziptie_1")
        res_model.connect("ziptie_1", "ziptie_2")
        res_model.connect("ziptie_2", "ziptie_3")

        for i_ziptie in np.arange(n_zipties_per_res - 1, -1, -1):
            z_width = .9 / n_zipties_per_res
            z_height = .9 / n_resolutions
            z_left = i_ziptie / n_zipties_per_res + .05 * z_width
            z_bottom = (2 - i_res) / n_resolutions + .05 * z_height

            ziptie = res_model.blocks[f"ziptie_{i_ziptie}"]
            for i_row in range(n_feature_rows):
                for i_col in range(n_feature_cols):
                    # Create a single active feature signal.
                    n_features = ziptie.algo.n_bundles
                    i_feature = np.random.choice(n_features)
                    feature_activities = np.zeros(n_features)
                    feature_activities[int(i_feature)] = 1

                    img = res_model.backward_pass(feature_activities) * 4 + .5
                    img = np.maximum(img, 0)
                    img = np.minimum(img, 1)

                    width = z_width / n_feature_cols
                    height = z_height / n_feature_rows
                    left = z_left + i_col * width
                    bottom = z_bottom + i_row * height
                    ax = fig.add_axes((left, bottom, width, height))
                    ax.imshow(img, vmin=0, vmax=1)
                    ax.tick_params(
                        bottom=False,
                        top=False,
                        left=False,
                        right=False)
                    ax.tick_params(
                        labelbottom=False,
                        labeltop=False,
                        labelleft=False,
                        labelright=False)

            res_model.remove(f"ziptie_{i_ziptie}")

    plt.savefig(os.path.join(image_path, figure_filename))
    plt.close()


if __name__ == "__main__":
    sample_features(model_filename)
