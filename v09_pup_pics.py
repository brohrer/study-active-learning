import os
import pickle as pkl
import sys
import termios
import tty
import numpy as np
import matplotlib.pyplot as plt
from image_diff import ImageDiff
from cottonwood.operations import Copy, Flatten, Stack
from cottonwood.structure import Structure
from cottonwood.experimental.ziptie_block import Ziptie
from cottonwood.experimental.naive_bayes import NaiveBayes
from cottonwood.pooling import AvgPool2D
from v08_feature_report import generate_report
from v10_data_tools import retrieve_model, save_model, load_images, get_image_patch

# n_bundles_str = "10"
# n_bundles_str = "100"
# n_bundles_str = "200"
n_bundles_str = "300"
# n_bundles_str = "1000"

# threshold_str = "1"
threshold_str = "10"
# threshold_str = "100"
# threshold_str = "1000"

n_d_bundles = int(n_bundles_str)
n_s_bundles = int(n_bundles_str)
threshold = int(threshold_str)

model_filename = f"model_imagenette_{n_bundles_str}b_{threshold_str}t.pkl"
model_supervised_filename = (
    f"model_imagenette_super_{n_bundles_str}b_{threshold_str}t.pkl")
image_dir = "pics"
training_figfile = "training.png"

n_iter_train = int(5e4)
n_iter_test = int(1e4)
n_iter_update = int(1e3)


def main():
    images = load_images()
    if os.path.exists(model_filename):
        model = retrieve_model(model_filename)
    else:
        model = initialize_model()
        train_unsupervised(model, images)
        save_model(model, model_filename)

    train_supervised(model, images)
    test_supervised(model, images)
    save_model(model, model_supervised_filename)


def initialize_model():
    model = Structure()
    model.add(ImageDiff(), "diff")
    model.add(AvgPool2D(window=2, stride=2), "pool_d")
    model.add(Flatten(), "flatten_d")
    model.add(Ziptie(n_outputs=n_d_bundles, threshold=threshold), "ziptie_d_0")

    model.connect("diff", "pool_d")
    model.connect("pool_d", "flatten_d")
    model.connect("flatten_d", "ziptie_d_0")
    return model


def train_unsupervised(model, images):
    i_iter = 0
    patch_generator = get_image_patch(images)
    while not model.blocks["ziptie_d_0"].is_full():
        i_iter += 1
        patch, center = next(patch_generator)
        model.forward_pass(patch)
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_d_0"].algo.n_bundles,
                "z0 bundles deriv at iter",
                i_iter)

    model.add(Copy(), "copy_d_0")
    model.add(Ziptie(n_outputs=n_d_bundles, threshold=threshold), "ziptie_d_1")
    model.connect("ziptie_d_0", "copy_d_0")
    model.connect("copy_d_0", "ziptie_d_1")

    while not model.blocks["ziptie_d_1"].is_full():
        i_iter += 1
        patch, center = next(patch_generator)
        model.forward_pass(patch)
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_d_1"].algo.n_bundles,
                "z1 bundles deriv at iter",
                i_iter)

    model.add(Copy(), "copy_d_1")
    model.add(Ziptie(n_outputs=n_d_bundles, threshold=threshold), "ziptie_d_2")
    model.connect("ziptie_d_1", "copy_d_1")
    model.connect("copy_d_1", "ziptie_d_2")

    while not model.blocks["ziptie_d_2"].is_full():
        i_iter += 1
        patch, center = next(patch_generator)
        model.forward_pass(patch)
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_d_2"].algo.n_bundles,
                "z2 bundles deriv at iter",
                i_iter)

    model.add(Copy(), "copy_d_2")
    model.add(Ziptie(n_outputs=n_d_bundles, threshold=threshold), "ziptie_d_3")
    model.connect("ziptie_d_2", "copy_d_2")
    model.connect("copy_d_2", "ziptie_d_3")

    while not model.blocks["ziptie_d_3"].is_full():
        i_iter += 1
        patch, center = next(patch_generator)
        model.forward_pass(patch)
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_d_3"].algo.n_bundles,
                "z3 bundles deriv at iter",
                i_iter)
    return model


def train_supervised(model, images):

    # Set up to grab keypresses
    orig_settings = termios.tcgetattr(sys.stdin)
    tty.setcbreak(sys.stdin)
    key = 0

    # Set up example image
    fig_ui = plt.figure()
    plt.ion()
    plt.show()
    ax_ui = fig_ui.gca()
    ax_ui.imshow(np.zeros((5, 5)))
    plt.draw()
    plt.pause(0.001)
    print()

    model.add(Stack(), "stack_d_0_1")
    model.add(Stack(), "stack_d_2_3")
    model.add(Stack(), "stack_d_all")
    model.add(NaiveBayes(n_categories=11), "naive_bayes")

    model.connect("copy_d_0", "stack_d_0_1", i_port_head=0, i_port_tail=1)
    model.connect("copy_d_1", "stack_d_0_1", i_port_head=1, i_port_tail=1)
    model.connect("copy_d_2", "stack_d_2_3", i_port_head=0, i_port_tail=1)
    model.connect("ziptie_d_3", "stack_d_2_3", i_port_head=1, i_port_tail=0)
    model.connect("stack_d_0_1", "stack_d_all", i_port_head=0, i_port_tail=0)
    model.connect("stack_d_2_3", "stack_d_all", i_port_head=1, i_port_tail=0)
    model.connect("stack_d_all", "naive_bayes")

    patch_generator = get_image_patch(images)
    misses = []
    accuracies = []
    n_labels = 0
    for i_iter in range(n_iter_train):
        patch, center = next(patch_generator)
        model.forward_pass(patch)

        # If less confident, get a label
        confidence = float(model.blocks["naive_bayes"].confidence)
        confidence_buffer = 1 / (n_labels + 1)
        if np.random.sample() < 2 ** -confidence + confidence_buffer:
            get_label(model, image, center)
        if (i_iter + 1) % n_iter_update == 0:

            recent_misses = np.array(misses[-n_iter_update:])
            accuracy = 1 - np.mean(recent_misses)
            accuracies.append(accuracy)
            print(
                f"{np.sum(recent_misses)} out of " +
                f"{recent_misses.size} digits misclassified " +
                f"at iteration {i_iter + 1} " +
                f"with {n_labels} labels " +
                f"for an accuracy of {accuracy * 100:.04} percent.")
            iterations = (np.arange(len(accuracies)) + 1) * n_iter_update
            errors = (1 - np.array(accuracies)) * 100

            fig = plt.figure()
            ax = fig.gca()
            ax.plot(iterations, errors, color="#04253a")
            ax.set_xlabel("Iteration")
            ax.set_ylabel("Training error")
            ax.set_ylim(0, 50)
            ax.grid()
            plt.savefig(training_figfile, dpi=300)
            plt.close()

    # Undo keypress grabbing
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, orig_settings)

    return model


def get_label(model, image, center):
    ax_ui.clear()
    ax_ui.imshow(image)
    draw_crosshairs(
        ax_ui,
        center,
        radius=20)
    ax_ui.set_xlim(-.5, image.shape[1] - .5)
    ax_ui.set_ylim(image.shape[0] - .5, -.5)
    ax_ui.set_title(
        f"{model.blocks['naive_bayes'].forward_out} " +
        f"conf {confidence}")
    ax_ui.set_xlabel("target: 0,   not target: 1", fontsize=8)
    plt.draw()
    plt.pause(0.01)

    keypress = sys.stdin.read(1)[0]
    print(keypress + "               ", end="\r")
    if keypress == "z":
        print("    Undo", end="\r")
        model.blocks["naive_bayes"].remove_last_observation()

        keypress = sys.stdin.read(1)[0]
        print(keypress + "               ", end="\r")

    if keypress == "b":
        plt.close()
        plt.pause(0.01)
        print("\n    Moving on")
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, orig_settings)
        return model

    if keypress not in ["0", "1"]:
        return

    key_category = int(keypress)
    n_labels += 1
    model.blocks["naive_bayes"].add_labeled_observation(key_category)

    try:
        if (key_category == model.blocks["naive_bayes"].forward_out):
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass


def draw_crosshairs(ax, center, radius=10):
    linewidth_white = 2
    linewidth_black = 1
    zorder = 5
    t = np.linspace(0, 2.01 * np.pi, 100)
    yc, xc = center
    x = np.cos(t) * radius + xc
    y = np.sin(t)* radius + yc
    ax.plot(x, y, color="white", linewidth=linewidth_white, zorder=zorder)
    ax.plot(
        [xc, xc],
        [yc + radius * .5, yc + radius * 1.5],
        color="white",
        linewidth=linewidth_white,
        solid_capstyle="round",
        zorder=zorder)
    ax.plot(
        [xc, xc],
        [yc - radius * .5, yc - radius * 1.5],
        color="white",
        linewidth=linewidth_white,
        solid_capstyle="round",
        zorder=zorder)
    ax.plot(
        [xc + radius * .5, xc + radius * 1.5],
        [yc, yc],
        color="white",
        linewidth=linewidth_white,
        solid_capstyle="round",
        zorder=zorder)
    ax.plot(
        [xc - radius * .5, xc - radius * 1.5],
        [yc, yc],
        color="white",
        linewidth=linewidth_white,
        solid_capstyle="round",
        zorder=zorder)

    ax.plot(x, y, color="black", linewidth=linewidth_black, zorder=zorder)
    ax.plot(
        [xc, xc],
        [yc + radius * .5, yc + radius * 1.5],
        color="black",
        linewidth=linewidth_black,
        solid_capstyle="round",
        zorder=zorder)
    ax.plot(
        [xc, xc],
        [yc - radius * .5, yc - radius * 1.5],
        color="black",
        linewidth=linewidth_black,
        solid_capstyle="round",
        zorder=zorder)
    ax.plot(
        [xc + radius * .5, xc + radius * 1.5],
        [yc, yc],
        color="black",
        linewidth=linewidth_black,
        solid_capstyle="round",
        zorder=zorder)
    ax.plot(
        [xc - radius * .5, xc - radius * 1.5],
        [yc, yc],
        color="black",
        linewidth=linewidth_black,
        solid_capstyle="round",
        zorder=zorder)


if __name__ == "__main__":
    main()
